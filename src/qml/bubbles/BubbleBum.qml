import QtQuick 1.0
//import "gamelogic.js" as GameLogic

Item {
    id: bubblebum

    property bool wrapped: false
    property int type: 0

    Image {
        id: imgbum
        anchors {
            fill: parent; leftMargin: 5; bottomMargin: 5
        }
        //source: "../bubbles/pics/wraped.svg";
        source: {
            if (type == 0)
               "../bubbles/pics/wraped.svg";
            else if (type == 1)
                "../bubbles/pics/wrapped.svg";
            else if (type == 2)
                "../bubbles/pics/wrappped.svg";
            else
                "../bubbles/pics/wrapppped.svg";
        }
    }
}
