import QtQuick 1.0

Rectangle {
     id: container

     property string text: "Button"

     signal clicked

     width: 48; height: 48
     //border { width: 1; color: Qt.darker(activePalette.button) }
     smooth: true
     color: "orange"
     radius: 45

     Image {
         id: newgame
         anchors.fill: parent
         source: "../bubbles/pics/new_game.png"
     }

     MouseArea {
         id: mouseArea
         anchors.fill: parent
         onClicked: container.clicked();
     }
 }
