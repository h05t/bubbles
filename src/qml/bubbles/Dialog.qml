import QtQuick 1.0
import "gamelogic.js" as GameLogic

Rectangle {
     id: page

     property Item text: dialogText

     signal closed
     signal opened

     function close() {
         if(page.opacity == 0)  //already closed
             return;
         page.closed();
         page.opacity = 0;
     }

     function show(txt) {
         page.opened();
         dialogText.text = txt;
         page.opacity = 1;
     }

     width: 100; height: 125
     color: "white"
     border.width: 2
     radius: 5
     opacity: 0
     visible: opacity > 0

     Behavior on opacity {
         NumberAnimation { duration: 1000 }
     }

     Text {
         id: dialogText;
         anchors.centerIn: parent;
     }
 }
