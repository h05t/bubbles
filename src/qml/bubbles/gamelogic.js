// gamelogic.js
// Base game logic

// ------------------------------------------------------------------------
// VARIABLES
// ------------------------------------------------------------------------


//var blockSize = 65;   // maemo 5 & meego
var blockSize = 85;     // symbian 5800 & E7

var maxColumn = 5;
var maxRow = 8;
var maxIndex = maxColumn * maxRow;

var board = new Array(maxIndex);
var bumArray = new Array(maxIndex);

var wrapComponent = Qt.createComponent("BubbleWrap.qml");
var bumComponent = Qt.createComponent("BubbleBum.qml");

//var gameStarted = false;
var defaultTime = 60;
var updateTime = 200;
var startTime = 0;
var timer = 0;

var bubblesCounter = 0;
var bubblesNum = 0;


// ------------------------------------------------------------------------
// FUNCTIONS
// ------------------------------------------------------------------------

function startGame()
{
    //Calculate board size
    maxColumn = Math.floor(background.width / blockSize);
    maxRow = Math.floor(background.height / blockSize);
    maxIndex = maxRow * maxColumn;

    //Initialize Board
    board = new Array(maxIndex);
    bumArray = new Array(maxIndex);
    for (var column = 0; column < maxColumn; ++column)
        for (var row = 1; row < maxRow; ++row)
        {
            board[index(column, row)] = null;
            if (row % 2 == 0)
                createWrapBlock(column, row, -15);   // second
            else
                createWrapBlock(column, row, 29);   // first
        }

    // game properties
    gameOverText.opacity = 0;       // game over text
    gameCanvas.score = 0;
    gameCanvas.speed = 0;
    gameTimer.start();
    startTime = new Date();
    gameCanvas.time = 60;

    startSound.play();              // start game sound
    nameInputDialog.close();
    table.close();

}

function stopGame()
{
    //Delete blocks from previous game
    for (var i = 0; i < maxIndex; ++i)
        if (board[i] != null) {
            board[i].destroy();
            bumArray[i].destroy();
        }

    // Hide bum blocks from previous game
    //for (var column = 0; column < maxColumn; ++column)
    //    for (var row = 1; row < maxRow; ++row)
    //        bumArray[index(column, row)].opacity = 0;

    //gameCanvas.enabled = false;     // block game canvas for touch
    //gameStarted = false;
    bubblesCounter = 0;
    bubblesNum = 0;
    gameTimer.stop();
    overSound.play();               // game over sound 
}

// update timer actions
function onUpdate()
{
    timer = 10 - ((new Date() - startTime) / 1000);
    gameCanvas.time = Math.floor(timer);

    increaseSpeed();
    decreaseSpeed();

    if ((gameCanvas.time == 0) || (bubblesNum == bubblesCounter))
    {
        increaseSpeedScore();
        stopGame();

        nameInputDialog.show("Your name:                 ");
        nameInputDialog.initialWidth = nameInputDialog.text.width + 20;
        if(nameInputDialog.name == "")
            nameInputDialog.width = nameInputDialog.initialWidth;
        nameInputDialog.text.opacity = 0;
    }
}

// game speed
function getUpdateTime()
{
    return updateTime;
}

// create wrap bubble
function createWrapBlock(column, row, xn)
{
    if (wrapComponent.status == Component.Ready)
    {
        var bubbleWrapObject = wrapComponent.createObject(gameCore);
        if (bubbleWrapObject == null)
        {
            console.log("error creating block");
            console.log(wrapComponent.errorString());
            return false;
        }
        bubbleWrapObject.x = (column * blockSize) + xn;
        bubbleWrapObject.y = row * blockSize;
        bubbleWrapObject.width = blockSize;
        bubbleWrapObject.height = blockSize;
        bubbleWrapObject.wrap = true;
        board[index(column, row)] = bubbleWrapObject;
        ++bubblesNum;
    }
    else
    {
        console.log("error loading block component");
        console.log(wrapComponent.errorString());
        return false;
    }

    return true;
}

// create bum bubbles
function createBumBlock(column, row, xn)
{
    if (bumComponent.status == Component.Ready)
    {
        var bubbleBumObject = bumComponent.createObject(gameCore);
        if (bubbleBumObject == null)
        {
            console.log("error creating block");
            console.log(bumComponent.errorString());
            return false;
        }
        bubbleBumObject.type = Math.floor(Math.random() * 4);
        bubbleBumObject.x = (column * blockSize) + xn;
        bubbleBumObject.y = row * blockSize;
        bubbleBumObject.width = blockSize;
        bubbleBumObject.height = blockSize;
        bubbleBumObject.wrapped = true;
        //board[index(column, row)] = bubbleBumObject;
        bumArray[index(column, row)] = bubbleBumObject;
        //console.log(bumArray[index(column, row)]);
    }
    else
    {
        console.log("error loading block component");
        console.log(bumComponent.errorString());
        return false;
    }

    return true;
}

// canvas click handling
function handleClick(xPos, yPos)
{
    var column = Math.floor(xPos / gameCanvas.blockSize);
    var row = Math.floor(yPos / gameCanvas.blockSize);

    if (column >= maxColumn || column < 0 || row >= maxRow || row < 0)
        return;
    if (board[index(column, row)] == null)
        return;

    // increase score
    if (board[index(column, row)].wrap == true)
    {
        gameCanvas.score += 3;
        ++bubblesCounter;
        playSounds(column, row);                // play sounds
    }

    board[index(column, row)].opacity = 0;      // hide wrap bubble
    board[index(column, row)] = null;
    changeWrapToBum(column, row);               // change bubbles    
    bumArray[index(column, row)] = null;
}

function getDB()
{
    return openDatabaseSync("BubblesDBase", "1.0", "Bubbles High Scores", 1000000);
}

function fillDB()
{
    getDB().transaction(function(tx) {
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Kirill", "250", "10"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["John", "230", "15"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Ivan", "200", "20"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Alex", "180", "25"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Sofia", "150", "30"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Eva", "120", "35"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Egor", "100", "40"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Pavel", "80", "45"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Oleg", "60", "50"]);
       tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", ["Kim", "40", "55"]);
    });
}

function deletePlayers()
{
    getDB().transaction(function(tx) {
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Kirill", "250", "10"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["John", "230", "15"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Ivan", "200", "20"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Alex", "180", "25"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Sofia", "150", "30"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Eva", "120", "35"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Egor", "100", "40"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Pavel", "80", "45"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Oleg", "60", "50"]);
       tx.executeSql("DELETE FROM Scores WHERE name=? AND score=? AND time=?", ["Kim", "40", "55"]);
    });
}

// разделать на несколько и прорефакторить
// save users score
function saveUserScore(name)
{
     getDB().transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS Scores(name TEXT, score NUMBER, time NUMBER)');
        tx.executeSql("INSERT INTO Scores VALUES(?, ?, ?)", [name, gameCanvas.score, defaultTime - gameCanvas.time]);

        fillDB();

        var rs = tx.executeSql('SELECT * FROM Scores ORDER BY score desc, time asc LIMIT 10');

        nameTxt0.text = rs.rows.item(0).name;
        nameTxt1.text = rs.rows.item(1).name;
        nameTxt2.text = rs.rows.item(2).name;
        nameTxt3.text = rs.rows.item(3).name;
        nameTxt4.text = rs.rows.item(4).name;
        nameTxt5.text = rs.rows.item(5).name;
        nameTxt6.text = rs.rows.item(6).name;
        nameTxt7.text = rs.rows.item(7).name;
        nameTxt8.text = rs.rows.item(8).name;
        nameTxt9.text = rs.rows.item(9).name;

        scoreTxt0.text = rs.rows.item(0).score;
        scoreTxt1.text = rs.rows.item(1).score;
        scoreTxt2.text = rs.rows.item(2).score;
        scoreTxt3.text = rs.rows.item(3).score;
        scoreTxt4.text = rs.rows.item(4).score;
        scoreTxt5.text = rs.rows.item(5).score;
        scoreTxt6.text = rs.rows.item(6).score;
        scoreTxt7.text = rs.rows.item(7).score;
        scoreTxt8.text = rs.rows.item(8).score;
        scoreTxt9.text = rs.rows.item(9).score;

        timeTxt0.text = rs.rows.item(0).time;
        timeTxt1.text = rs.rows.item(1).time;
        timeTxt2.text = rs.rows.item(2).time;
        timeTxt3.text = rs.rows.item(3).time;
        timeTxt4.text = rs.rows.item(4).time;
        timeTxt5.text = rs.rows.item(5).time;
        timeTxt6.text = rs.rows.item(6).time;
        timeTxt7.text = rs.rows.item(7).time;
        timeTxt8.text = rs.rows.item(8).time;
        timeTxt9.text = rs.rows.item(9).time;

        table.show();
     }
     );
}

//Index function used instead of a 2D array
function index(column, row)
{
    return column + (row * maxColumn);
}

// increase users speed motion
function increaseSpeed()
{
    if (bubblesCounter == 5 && timer >= 50) gameCanvas.speed = 1;
    else if (bubblesCounter == 10 && timer >= 45) gameCanvas.speed = 2;
    else if (bubblesCounter == 15 && timer >= 40) gameCanvas.speed = 3;
    else if (bubblesCounter == 20 && timer >= 35) gameCanvas.speed = 4;
    else if (bubblesCounter == 24 && timer >= 30) gameCanvas.speed = 5;
}

// decrease users speed motion
function decreaseSpeed()
{
    if (bubblesCounter == 5 && timer <= 50) gameCanvas.speed = 0;
    else if (bubblesCounter == 10 && timer <= 45) gameCanvas.speed = 1;
    else if (bubblesCounter == 15 && timer <= 35) gameCanvas.speed = 2;
    else if (bubblesCounter == 20 && timer <= 25) gameCanvas.speed = 2;
    else if (bubblesCounter == 24 && timer <= 15) gameCanvas.speed = 3;
}

// increase score for speed bonus
function increaseSpeedScore()
{
    // увеличиваем очки с учетом бонуса скорости для t=60
    if (gameCanvas.speed == 1) gameCanvas.score *= 1.5;
    else if (gameCanvas.speed == 2) gameCanvas.score *= 2;
    else if (gameCanvas.speed == 3) gameCanvas.score *= 2.5;
    else if (gameCanvas.speed == 4) gameCanvas.score *= 3;
    else if (gameCanvas.speed == 5) gameCanvas.score *= 3.5;
    else gameCanvas.score *= 1;
}

// change wrap bubble to bum
function changeWrapToBum(column, row)
{
    // position over bubbles
    if (row % 2 == 0)
        if (column % 2 == 0)
            if (bubblesCounter % 2 == 0)
                createBumBlock(column, row, -15);
            else
                createBumBlock(column, row, -15);
        else
            if (bubblesCounter % 2 == 0)
                createBumBlock(column, row, -15);
            else
                createBumBlock(column, row, -15);
    else
        if (column % 2 == 0)
            if (bubblesCounter % 2 == 0)
                createBumBlock(column, row, 29);
            else
                createBumBlock(column, row, 29);
        else
            if (bubblesCounter % 2 == 0)
                createBumBlock(column, row, 29);
            else
                createBumBlock(column, row, 29);
}

// play custom sounds
function playSounds(column, row)
{
    if (column % 2 == 0)
        if (row % 2 == 0)
            wrapSound1.play();
        else
            wrapSound2.play();
    else
        if (row % 2 == 1)
            wrapSound3.play();
        else
            wrapSound4.play();
}

