import QtQuick 1.0
import Qt 4.7
import QtMultimediaKit 1.1
import "gamelogic.js" as GameLogic

Rectangle {
    id: screen

    //width: 480    // maemo 5 & meego
    //height: 800
    width: 360      // E7, N8
    height: 640

    SystemPalette { id: activePalette }

    // game canvas
    Item {
        id: gameCore

        width: parent.width
        height:  parent.height
        anchors.bottom: screen.bottom

        Image {
            id: background
            anchors.fill: parent
            source: "../bubbles/pics/fone.png"
            fillMode: Image.PreserveAspectCrop
        }

        Item {
            id: gameCanvas
            property int score: 0
            property int time: 60
            property int speed: 0
            property int level: 0
            //property int blockSize: 65    // maemo 5 & meego
            property int blockSize: 85      // symbian 5800 & E7

            width: parent.width - (parent.width % blockSize)
            height: parent.height - (parent.height % blockSize)
            z: 20;
            anchors.centerIn: parent

            // start sound
            SoundEffect {
                id: startSound
                source: "../bubbles/sounds/startup.mp3"
            }
            // game over sound
            SoundEffect {
                id: overSound
                source: "../bubbles/sounds/game_over.mp3"
            }

            // bubble bum sounds
            SoundEffect {
                id: wrapSound1
                source: "../bubbles/sounds/bum_one.mp3"
            }
            SoundEffect {
                id: wrapSound2
                source: "../bubbles/sounds/bum_two.mp3"
            }
            SoundEffect {
                id: wrapSound3
                source: "../bubbles/sounds/bum_three.mp3"
            }
            SoundEffect {
                id: wrapSound4
                source: "../bubbles/sounds/bum_four.mp3"
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onPressed: { GameLogic.handleClick(mouse.x, mouse.y) }
            }
        }
    }

    // scores tables
    Rectangle {
        id: table

        signal closed
        signal opened

        function close() {
            if(table.opacity == 0) return; //already closed
            table.closed();
            table.opacity = 0;

            logoLbl.enabled = true; logoLbl.opacity = 1;
            gameOverText.opacity = 1;       // game over text
            GameLogic.deletePlayers();
        }

        function show() {
            table.opened();
            table.opacity = 1;
        }

        width: 310; height: 340; radius: 10; z: 21
        anchors.centerIn: parent
        border { width: 2; color: "black" }
        //color: "white"
        opacity: 0; visible: opacity > 0

        Behavior on opacity { NumberAnimation { duration: 1000 } }

        Text { id: tableTxt; text: "TOP 10: "; anchors { top: table.top; topMargin: 5; left: table.left; leftMargin: 10 } font { pointSize: 8; bold: true } }

        // button X
        Rectangle {
            id: closeButton
            width: 60; height: 30
            anchors {
                top: table.top; topMargin: 4
                right: table.right; rightMargin: 12
            }
            border { width: 2; color: "black" }
            radius: 5; color: "black"

            Text { id: closeText; anchors.centerIn: parent; text: "X"; color: "white" }

            MouseArea { anchors.fill: parent; onClicked: table.close(); }
        }

        // indish table
        Grid {
            id: scoresTable
            anchors { top: tableTxt.bottom; topMargin: 5; left: tableTxt.left; leftMargin: 1 }
            columns: 3
            rows: 11

            Rectangle { id: nameTbl; width:  172; height: 25; border.width: 2; color: "orange"; Text { text: "Name"; anchors.centerIn: parent; color: "white"; font { pointSize: 7; bold: true } } }
            Rectangle { id: scoreTbl; width: 65; height: 25; border.width: 2; color: "orange"; Text { text: "Score"; anchors.centerIn: parent; color: "white"; font { pointSize: 7; bold: true } } }
            //Rectangle { id: levelTbl; width: 50; height: 25; border.width: 2; color: "orange"; Text { text: "Level"; anchors.centerIn: parent; color: "white"; font { pointSize: 7; bold: true } } }
            Rectangle { id: timeTbl; width: 50; height: 25; border.width: 2; color: "orange"; Text { text: "Time"; anchors.centerIn: parent; color: "white"; font { pointSize: 7; bold: true } } }

            Rectangle { id: nameCell1; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt0; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell1; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt0; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell1; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt0; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell1; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt0; anchors.centerIn: parent;text: ""; } }

            Rectangle { id: nameCell2; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt1; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell2; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt1; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell2; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt1; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell2; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt1; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell3; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt2; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell3; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt2; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell3; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt3; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell3; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt2; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell4; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt3; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell4; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt3; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell4; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt3; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell4; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt3; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell5; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt4; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell5; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt4; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell5; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt4; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell5; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt4; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell6; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt5; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell6; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt5; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt5; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell6; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt5; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell7; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt6; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell7; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt6; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell7; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt6; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell7; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt6; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell8; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt7; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell8; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt7; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell8; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt7; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell8; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt7; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell9; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt8; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell9; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt8; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell9; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt8; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell9; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt8; anchors.centerIn: parent; text: ""; } }

            Rectangle { id: nameCell10; width:  172; height: 27; border.width: 2; color: "transparent"; Text { id: nameTxt9; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: scoreCell10; width: 65; height: 27; border.width: 2; color: "transparent"; Text { id: scoreTxt9; anchors.centerIn: parent; text: ""; } }
            //Rectangle { id: levelCell10; width: 50; height: 25; border.width: 2; color: "transparent"; Text { id: levelTxt9; anchors.centerIn: parent; text: ""; } }
            Rectangle { id: timeCell10; width: 50; height: 27; border.width: 2; color: "transparent"; Text { id: timeTxt9; anchors.centerIn: parent; text: ""; } }
        }
    }

    // name dialog
    Dialog {
        id: nameInputDialog

        property int initialWidth: 0
        property alias name: nameInputText.text

        anchors.centerIn: parent
        z: 22;

        Behavior on width {
            NumberAnimation {}
            enabled: nameInputDialog.initialWidth != 0
        }

        onOpened: nameInputText.focus = true;

        onClosed: {
            nameInputText.focus = false;
            if (nameInputText.text != "")
                 GameLogic.saveUserScore(nameInputText.text);
        }

        Text {
            id: dialogText
            anchors {
                left: nameInputDialog.left; leftMargin: 10;
                top: nameInputDialog.top; topMargin: 15
            }
            text: "Your name: "
        }

        TextInput {
            id: nameInputText
            anchors {
                top: nameInputDialog.top; topMargin: 15
                left: dialogText.right
            }
            focus: false
            autoScroll: false
            maximumLength: 20

            onTextChanged: {
                var newWidth = nameInputText.width + dialogText.width + 40;
                if ((newWidth > nameInputDialog.width && newWidth < screen.width) ||
                    (nameInputDialog.width > nameInputDialog.initialWidth))
                    nameInputDialog.width = newWidth;
            }

            onAccepted: { nameInputDialog.close(); }
        }

        // button OK
        Rectangle {
            id: enterButton
            width: 60; height: 40
            anchors {
                //top: nameInputText.bottom; topMargin: 5
                bottom: nameInputDialog.bottom; bottomMargin: 4
                right: nameInputDialog.right; rightMargin: 4
            }
            border { width: 2; color: "black" }
            radius: 5; color: "black"

            Text { id: enterText; anchors.centerIn: parent; text: "OK"; color: "white" }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    if (nameInputText.text == "")
                        nameInputText.openSoftwareInputPanel();
                    else {
                        nameInputDialog.close();
                    }
                }
            }
        }
    }

    // game timer
    Timer {
        id: gameTimer
        interval: GameLogic.getUpdateTime()
        repeat: true
        running: false
        triggeredOnStart: false
        onTriggered: GameLogic.onUpdate()
    }

    // status bar
    Rectangle {
        id: toolBar

        width: parent.width - 10
        height: 50
        color: "orange"
        anchors {
            topMargin: 5;
            horizontalCenter: screen.horizontalCenter;
            top: screen.top
        }
        border.width: 1
        border.color: "white"
        radius: 15

        // time
        Image {
            id: timeImg
            anchors {
                left: parent.left;
                leftMargin: 5;
                verticalCenter: parent.verticalCenter
            }
            source: "../bubbles/pics/time.png"
        }

        Text {
            id: timeTxt
            anchors {
                left: timeImg.right;
                verticalCenter: parent.verticalCenter
            }
            text: ": "  + gameCanvas.time

            //font { family: "Comic Sans MS"; pointSize: 16; bold: true }   // maemo 5 & meego
            font { family: "Comic Sans MS"; pointSize: 8; bold: true }      // symbian 5800 & E7

            color: "white"
        }

        // score
        Image {
            id: scoreImg
            anchors {
                left: timeTxt.right;
                leftMargin: 5;
                verticalCenter: parent.verticalCenter
            }
            source: "../bubbles/pics/score.png"
        }

        Text {
            id: scoreTxt
            anchors {
                left: scoreImg.right;
                verticalCenter: parent.verticalCenter
            }
            text: ": " + gameCanvas.score

            //font { family: "Comic Sans MS"; pointSize: 16; bold: true }   // maemo 5 & meego
            font { family: "Comic Sans MS"; pointSize: 8; bold: true }      // symbian 5800 & E7

            color: "white"
        }

        // speed
        Image {
            id: speedImg
            anchors {
                left: scoreTxt.right;
                leftMargin: 5;
                verticalCenter: parent.verticalCenter
            }
            source: "../bubbles/pics/speed.png"
        }

        Text {
            id: speedTxt
            anchors {
                left: speedImg.right;
                verticalCenter: parent.verticalCenter
            }
            text: ": " + gameCanvas.speed + "/5"

            //font { family: "Comic Sans MS"; pointSize: 16; bold: true }   // maemo 5 & meego
            font { family: "Comic Sans MS"; pointSize: 7; bold: true }      // symbian 5800 & E7
            color: "white"
        }

        // level
        /*Image {
            id: levelImg
            anchors {
                left: speedTxt.right;
                leftMargin: 5;
                verticalCenter: parent.verticalCenter
            }
            source: "../bubbles/pics/level.png"
        }

        Text {
            id: levelTxt
            anchors {
                left: levelImg.right;
                verticalCenter: parent.verticalCenter
            }
            text: ": " + gameCanvas.level

            //font { family: "Comic Sans MS"; pointSize: 16; bold: true }   // maemo 5 & meego
            font { family: "Comic Sans MS"; pointSize: 7; bold: true }      // symbian 5800 & E7
            color: "white"
        }
        */
        // exit status button
        Image {
            id: exitButton
            width: 48; height: 48
            anchors {
                right: parent.right;
                leftMargin: 5;
                verticalCenter: parent.verticalCenter
            }
            source: "../bubbles/pics/exit.png"

            MouseArea {
                id: buttonArea
                anchors.fill: parent
                onClicked: exitDialog.show("                                                     ");
            }
        }
    }

    // exit dialog
    Dialog {
        id: exitDialog
        width: exitText.width + 20; height: exitText.height + 60
        anchors.centerIn: parent
        z: 23;

        Text {
            id: exitText
            anchors {
                left: exitDialog.left; leftMargin: 10;
                top: exitDialog.top; topMargin: 10;
            }
            text: "Are you really want to quit?"
        }

        // button Yes
        Rectangle {
            id: okButton
            width: 60; height: 40
            anchors {
                bottom: exitDialog.bottom; bottomMargin: 3
                left: exitDialog.left; leftMargin: 5
            }
            color: "black"
            border.width: 2
            radius: 5

            Text { id: okText; anchors.centerIn: parent; color: "white"; text: "Yes" }

            MouseArea { anchors.fill: parent; onClicked: Qt.quit(); }
        }

        // button No
        Rectangle {
            id: noButton
            width: 60; height: 40
            anchors {
                bottom: exitDialog.bottom; bottomMargin: 3
                right: exitDialog.right; rightMargin: 5
            }
            color: "black"
            border.width: 2
            radius: 5

            Text { id: noText; anchors.centerIn: parent; color: "white"; text: "No" }

            MouseArea { anchors.fill: parent; onClicked: exitDialog.close(); }
        }
    }

    // enter logo
    Text {
        id: logoLbl
        anchors.centerIn: parent
        text: "Please touch to start"
        //opacity: 0
        visible: opacity > 0
        Behavior on opacity { NumberAnimation { duration: 500 } }

        //font { family: "Comic Sans MS"; pointSize: 20; bold: true }   // maemo 5 & meego
        font { family: "Comic Sans MS"; pointSize: 11; bold: true }     // symbian 5800 & E7
        color: "gray"

        MouseArea {
            id: textTouchArea
            anchors.fill: parent
            onPressed: {
                logoLbl.enabled = false; logoLbl.opacity = 0;
                GameLogic.startGame();
            }
        }
    }

    // game over text
    Text {
        id: gameOverText
        anchors {
            top: parent.top; 
            topMargin: 70;
            horizontalCenter: parent.horizontalCenter
        }
        text: "Game Over"
        opacity: 0
        visible: opacity > 0

        Behavior on opacity { NumberAnimation { duration: 500 } }

        //font { family: "Comic Sans MS"; pointSize: 24; bold: true }   // maemo 5 & meego
        font { family: "Comic Sans MS"; pointSize: 13; bold: true }     // symbian 5800 & E7

        color: "red"
    }
}
