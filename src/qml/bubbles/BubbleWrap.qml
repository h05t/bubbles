import QtQuick 1.0
import "gamelogic.js" as GameLogic

Item {
     id: bubblewrap

     property bool wrap: false

     Behavior on width { SpringAnimation{ spring: 2; damping: 0.2 } }

     Behavior on height { SpringAnimation{ spring: 2; damping: 0.2 } }

     Image {
         id: imgwrap
         anchors.leftMargin: 5
         anchors.bottomMargin: 5
         anchors.fill: parent
         source: "../bubbles/pics/bubble.svg"
     }
}
